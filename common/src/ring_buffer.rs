
/// A circular buffer.
#[derive(Debug, Clone)]
pub struct RingBuffer<T> {
    /// Data of the buffer; can be empty at creation, then it will grow until it reaches `size`.
    data: Vec<T>,
    /// Size of the buffer.
    length: usize,
    /// Actual read pointer.
    read_ptr: usize,
    /// Actual size of available data (always <= length).
    read_length: usize,
}

impl<T> RingBuffer<T>
    where T: Clone + Copy + Default
{
    pub fn new(length: usize) -> Self {
        Self {
            data: vec![T::default(); length],
            length,
            read_ptr: 0,
            read_length: 0,
        }
    }

    /// Returns the size of the buffer.
    pub fn len(&self) -> usize {
        self.length
    }

    pub fn read_length(&self) -> usize {
        self.read_length
    }

    pub fn available_length(&self) -> usize {
        self.length - self.read_length
    }

    /// Returns the raw data. The first element will certainly not be the oldest element pushed
    /// into the buffer.
    pub fn raw_data(&self) -> &[T] {
        self.data.as_slice()
    }

    pub fn is_full(&self) -> bool {
        self.read_length == self.length
    }

    /// Gets an element of the buffer. 0 is the oldest element.
    pub fn peek(&self, index: usize) -> Option<&T> {
        if index >= self.read_length {
            None
        } else {
            let offset = self.offset(index);
            Some(&self.data[offset])
        }
    }

    /// Pops an element of the buffer.
    pub fn pop(&mut self) -> Option<T> {
        if self.read_length == 0 {
            None
        } else {
            let offset = self.offset(0);
            let el = self.data[offset];
            self.advance_ptr(1);
            Some(el)
        }
    }

    /// Appends a new element into the buffer.
    pub fn push(&mut self, el: T) -> Result<(), ()> {
        if self.is_full() {
            Err(())
        } else {
            let offset = self.offset(self.read_length);
            self.data[offset] = el;
            self.read_length += 1;
            Ok(())
        }
    }

    pub fn push_slice(&mut self, slice: &[T]) -> Result<(), ()> {
        let slice_len = slice.len();
        if slice_len > self.available_length() {
            return Err(());
        }
        let start = self.offset(self.read_length);
        if start + slice_len > self.length {
            let right_size = self.length - start;
            let left_size = slice_len - right_size;
            let (left, right) = self.data.split_at_mut(start);
            right.copy_from_slice(&slice[0..right_size]);
            left[0..left_size].copy_from_slice(&slice[right_size..]);
        } else {
            self.data[start..start + slice_len].copy_from_slice(slice);
        }
        self.read_length += slice_len;
        Ok(())
    }

    pub fn pop_slice(&mut self, size: usize) -> Option<(&[T], Option<&[T]>)> {
        if size > self.read_length {
            return None;
        }
        let ret = if self.read_ptr + size > self.length {
            let right_size = self.length - self.read_ptr;
            let left_size = size - right_size;
            Some((
                &self.data[self.read_ptr..],
                Some(&self.data[0..left_size]),
            ))
        } else {
            Some((
                &self.data[self.read_ptr..self.read_ptr+size],
                None
            ))
        };
        self.read_ptr = self.offset(size);
        self.read_length -= size;
        ret
    }

    pub fn iter(&self) -> RingBufferIterator<'_, T> {
        RingBufferIterator::new(&self)
    }

    fn offset(&self, index: usize) -> usize {
        (self.read_ptr + index) % self.length
    }

    fn advance_ptr(&mut self, length: usize) {
        self.read_ptr = self.offset(length);
        self.read_length -= length;
    }

    /// Returns the actual read pointer.
    #[cfg(test)]
    pub fn read_ptr(&self) -> usize {
        self.read_ptr
    }
}

/// An iterator over the elements of a ring buffer.
pub struct RingBufferIterator<'a, T> {
    buffer: &'a RingBuffer<T>,
    pos: usize,
}

impl<'a, T> Iterator for RingBufferIterator<'a, T>
    where T: Clone + Copy + Default
{
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        let el = self.buffer.peek(self.pos);
        if el.is_some() {
            self.pos += 1;
        }
        el
    }
}

impl<'a, T> RingBufferIterator<'a, T> {
    pub fn new(buffer: &'a RingBuffer<T>) -> Self {
        Self {
            buffer,
            pos: 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_buffer() {
        let rb: RingBuffer<i32> = RingBuffer::new(4);
        assert!(!rb.is_full());
        assert_eq!(rb.len(), 4);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 0);
    }

    #[test]
    fn is_full() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(2);
        rb.push(10).unwrap();
        rb.push(10).unwrap();
        assert_eq!(rb.raw_data(), &[10, 10]);
        assert_eq!(rb.read_length(), 2);
        assert_eq!(rb.read_ptr(), 0);
        assert!(rb.is_full());
    }

    #[test]
    fn raw_data() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(2);
        rb.push(10).unwrap();
        rb.push(11).unwrap();
        assert_eq!(rb.raw_data(), &[10, 11]);
    }

    #[test]
    fn push() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push(10).unwrap();
        assert_eq!(rb.raw_data(), &[10, 0, 0, 0]);
        assert_eq!(rb.read_length(), 1);
        assert_eq!(rb.read_ptr(), 0);
        assert!(!rb.is_full());
    }

    #[test]
    fn pop_1() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push(10).unwrap();
        assert_eq!(rb.raw_data(), &[10, 0, 0, 0]);
        assert_eq!(rb.read_length(), 1);
        assert_eq!(rb.read_ptr(), 0);
        let n = rb.pop().unwrap();
        assert_eq!(rb.raw_data(), &[10, 0, 0, 0]);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(n, 10);
    }

    #[test]
    fn pop_2() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(3);
        rb.push(10).unwrap();
        let n = rb.pop().unwrap();
        assert_eq!(rb.raw_data(), &[10, 0, 0]);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(n, 10);
        rb.push(11).unwrap();
        let n = rb.pop().unwrap();
        assert_eq!(rb.raw_data(), &[10, 11, 0]);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 2);
        assert_eq!(n, 11);
        rb.push(12).unwrap();
        let n = rb.pop().unwrap();
        assert_eq!(rb.raw_data(), &[10, 11, 12]);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 0);
        assert_eq!(n, 12);
        rb.push(13).unwrap();
        let n = rb.pop().unwrap();
        assert_eq!(rb.raw_data(), &[13, 11, 12]);
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(n, 13);
    }

    #[test]
    fn push_too_much() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(2);
        rb.push(10).unwrap();
        rb.push(11).unwrap();
        assert!(rb.push(12).is_err());
        assert_eq!(rb.raw_data(), &[10, 11]);
        assert_eq!(rb.read_length(), 2);
        assert_eq!(rb.read_ptr(), 0);
        assert!(rb.is_full());
    }

    #[test]
    fn pop_too_much() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push(10).unwrap();
        rb.push(11).unwrap();
        assert_eq!(rb.raw_data(), &[10, 11, 0, 0]);
        assert_eq!(rb.read_length(), 2);
        assert_eq!(rb.read_ptr(), 0);
        let n = rb.pop().unwrap();
        assert_eq!(rb.read_length(), 1);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(n, 10);
        let n = rb.pop().unwrap();
        assert_eq!(rb.read_length(), 0);
        assert_eq!(rb.read_ptr(), 2);
        assert_eq!(n, 11);
        assert!(rb.pop().is_none());
    }

    #[test]
    fn push_slice_1() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push_slice(&[1, 2, 3]).unwrap();
        assert_eq!(rb.raw_data(), &[1, 2, 3, 0]);
        assert_eq!(rb.read_ptr(), 0);
        assert_eq!(rb.read_length(), 3);
    }

    #[test]
    fn push_slice_2() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push_slice(&[1, 2, 3]).unwrap();
        rb.pop().unwrap();
        rb.push_slice(&[4, 5]).unwrap();
        assert_eq!(rb.raw_data(), &[5, 2, 3, 4]);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(rb.read_length(), 4);
    }

    #[test]
    fn push_slice_3() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push_slice(&[1, 2, 3]).unwrap();
        assert!(rb.push_slice(&[4, 5]).is_err());
    }

    #[test]
    fn pop_slice_1() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push_slice(&[1, 2, 3]).unwrap();
        let (left, right) = rb.pop_slice(2).unwrap();
        assert_eq!(left, &[1, 2]);
        assert_eq!(right, None);
        assert_eq!(rb.read_ptr(), 2);
        assert_eq!(rb.read_length(), 1);
    }

    #[test]
    fn pop_slice_2() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(4);
        rb.push_slice(&[1, 2, 3]).unwrap();
        rb.pop_slice(2).unwrap();
        rb.push_slice(&[4, 5, 6]).unwrap();
        let (left, right) = rb.pop_slice(3).unwrap();
        assert_eq!(left, &[3, 4]);
        assert_eq!(right.unwrap(), &[5]);
        assert_eq!(rb.read_ptr(), 1);
        assert_eq!(rb.read_length(), 1);
    }

    #[test]
    fn empty_ring_buffer_iterator() {
        let rb: RingBuffer<i32> = RingBuffer::new(3);
        let values: Vec<i32> = rb.iter().copied().collect();
        assert!(values.is_empty());
    }

    #[test]
    fn ring_buffer_iterator() {
        let mut rb: RingBuffer<i32> = RingBuffer::new(3);
        rb.push(10).unwrap();
        rb.push(11).unwrap();
        rb.push(12).unwrap();
        rb.push(13).unwrap_err();

        let values: Vec<i32> = rb.iter().copied().collect();
        assert_eq!(values, &[10, 11, 12]);
    }
}
