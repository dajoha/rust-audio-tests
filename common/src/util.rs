pub fn gain_to_factor(gain: f64) -> f32 {
    10_f64.powf(gain / 20.0) as f32
}

pub fn time_to_frames(frame_rate: usize, time: f64) -> jack::Frames {
    (time * frame_rate as f64) as jack::Frames
}
