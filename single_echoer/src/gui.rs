use crossbeam_channel::Sender;
use gtk4::prelude::*;
use gtk4::{
    AdjustmentBuilder,
    Application,
    ApplicationWindow,
    Scale,
    ScaleBuilder,
    BoxBuilder,
    LabelBuilder,
    Orientation,
    Align,
    CssProvider,
    StyleContext,
    PositionType,
};

use crate::app_message::AppMessage;

fn init_gtk_css(win: &ApplicationWindow) {
    const CSS_CONTENT: &[u8] = b"
        scale {
            padding: 0;
        }
    ";
    let css = CssProvider::new();
    css.load_from_data(CSS_CONTENT);
    let display = win.display();
    StyleContext::add_provider_for_display(&display, &css, gtk4::STYLE_PROVIDER_PRIORITY_APPLICATION);
}

pub fn run_gtk_application(tx: Sender<AppMessage>) {
    let application = Application::builder()
        .application_id("yolenoyer.rust-audio-tests.single_echoer")
        .build();

    application.connect_activate(move |app| {
        let window = ApplicationWindow::builder()
            .application(app)
            .title("Single echoer")
            .default_width(600)
            .default_height(140)
            .build();

        init_gtk_css(&window);

        let win_box = BoxBuilder::new()
            .orientation(Orientation::Vertical)
            .margin_bottom(10)
            .margin_end(10)
            .margin_start(10)
            .margin_top(10)
            .build();

        // Delay:
        {
            let my_box = BoxBuilder::new()
                .orientation(Orientation::Vertical)
                .build();

            let label = LabelBuilder::new()
                .label("Delay")
                .halign(Align::Start)
                .build();

            let adjustment = AdjustmentBuilder::new()
                .lower(0.0)
                .upper(3_000.0)
                .value(1_000.0)
                .build();
            let tx = tx.clone();
            let slider = ScaleBuilder::new()
                .adjustment(&adjustment)
                .draw_value(true)
                .digits(0)
                .build();
            slider
                .connect("value-changed", true, move |values| {
                    let scale = values[0].get::<Scale>().unwrap();
                    tx.send(AppMessage::SetDelay(scale.value())).unwrap();
                    None
                })
                .unwrap();
            slider.set_format_value_func(|_slider, value| format!("{} ms", value));

            my_box.append(&label);
            my_box.append(&slider);
            win_box.append(&my_box);
        }

        // Decay:
        {
            let my_box = BoxBuilder::new()
                .orientation(Orientation::Vertical)
                .build();

            let label = LabelBuilder::new()
                .label("Decay")
                .halign(Align::Start)
                .build();

            let adjustment = AdjustmentBuilder::new()
                .lower(-40.0)
                .upper(30.0)
                .value(crate::INITIAL_DECAY)
                .build();
            let tx = tx.clone();
            let slider = ScaleBuilder::new()
                .adjustment(&adjustment)
                .draw_value(true)
                .digits(2)
                .build();
            slider
                .connect("value-changed", true, move |values| {
                    let scale = values[0].get::<Scale>().unwrap();
                    let gain = scale.value();
                    let factor = common::gain_to_factor(gain);
                    tx.send(AppMessage::SetDecayFactor(factor)).unwrap();
                    None
                })
                .unwrap();
            slider.set_format_value_func(|_slider, value| format!("{} dB", value));
            slider.add_mark(0.0, PositionType::Bottom, Some("0 dB"));

            my_box.append(&label);
            my_box.append(&slider);
            win_box.append(&my_box);
        }

        window.set_child(Some(&win_box));

        window.show();
    });

    application.run();
}
