#[derive(Debug)]
pub enum AppMessage {
    SetDelay(f64),
    SetDecayFactor(f32),
}
